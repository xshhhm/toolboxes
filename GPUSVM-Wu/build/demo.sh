#Run lasp-svm demo on the provided adult dataset
../build/train_mc --gpu -v 3 -s 2000 -c 1.0 -g 0.05 ../demo/adult.train ../demo/adult.model
../build/classify_mc ../demo/adult.test ../demo/adult.model ../demo/adult.labels