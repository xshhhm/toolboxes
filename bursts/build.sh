#!/bin/sh

CC=gcc
# CC="c99"

#other possibilities for the compiler:
# CC="clang"
# CC="sunc99"
# CC="icc -std=c99"
# CC="gcc -std=c99"
# CC="gcc -std=c99 -pedantic -Wall -Wextra -Werror -Wno-unused"


PROGRAMS="blur fft iion imprintf plambda qeasy ransac siftu srmatch synflow pview vecov lowe_join"


LFLAGS="-ljpeg -lpng -ltiff -lfftw3f"


$CC  -D_XOPEN_SOURCE=700 -c    src/iio.c   -o bin/iio.o

$CC  -D_XOPEN_SOURCE=700 -c    src/fmemopen.c  -o bin/fmemopen.o


for i in $PROGRAMS; do
	$CC src/$i.c          bin/fmemopen.o      bin/iio.o -o bin/$i $LFLAGS
done
ln -sf fft bin/ifft
